# Glossy Apertures

![small preview](previews/logo.png)

This is a theme that looks like a certain Sillicon Valley company that can be associated with the typical apertures that exist in houses and fences. It includes GTK2, GTK3 and GTK4. In particular it imitates the style it used some years ago, that appeared in an infamous version of one operative system of theirs. Despite the rejection said OS caused, I think its design was very good, and readable. So I tried to imitate some of its aspects.

This theme is based on two of my previous themes, Skewaita and DeeplySkeuo, which are based on Adwaita GTK3 and GTK4 (source sasscs!)

GTK2 is taken from [Aero42 by blue-dxca93](https://www.pling.com/p/1816038/).
Cinnamon and GnomeShell from [Aero by ELBULLAZUL](https://www.pling.com/p/1012735/).
Sadly, I can't tell where I got the xfwm4 theme from, sorry about that!


It is a skeuomorphic theme (that is, non-flat, that tries to imitate real-world objects and interfaces, like real three-dimensional buttons). It is a light theme but not too bright. Accents are sky blue. Disabled elements have a subtle red tint, in order to clearly identify their state. Buttons are glossy or shiny.

For the sake of making this theme more easily findable, I'll include these random words: aero, vista, windows.

Big previews:

![full desktop preview that includes gtk2, gtk3 and gtk4](previews/gtk3-gtk4-gtk2-desktop.png "Includes gtk2, gtk3 and gtk4")

![gtk3 widget page 2](previews/gtk3wf-2.png)

![gtk3 widget page 3](previews/gtk3wf-3.png)
